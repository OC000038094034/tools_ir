#################
# COMPILE IMAGE #
#################
FROM python:3.9-slim AS compile-image
RUN echo "ulimit -c 0" >> /etc/profile
RUN apt-get update && apt-get install -y --no-install-recommends \
  gcc \
  build-essential \
  && rm -rf /var/lib/apt/lists/*

RUN python -m venv /opt/venv
COPY /dependencies /dependencies

# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"

RUN pip3 install --no-cache-dir pip==24.3.1
COPY /requirements.txt /
RUN pip3 install --no-cache-dir -r requirements.txt

###############
# BUILD IMAGE #
###############
FROM python:3.9-slim AS iriswebapp
RUN echo "ulimit -c 0" >> /etc/profile
RUN pip uninstall -y setuptools

ENV PYTHONUNBUFFERED=1
COPY /dependencies /dependencies
COPY --from=compile-image /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"

RUN apt-get update && apt-get install -y --no-install-recommends \
  p7zip-full \
  pgpgpg \
  rsync \
  postgresql-client \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir /iriswebapp/ \
  && mkdir -p /home/iris/certificates/rootCA \
  && mkdir -p /home/iris/certificates/web_certificates \
  && mkdir -p /home/iris/user_templates \
  && mkdir -p /home/iris/server_data \
  && mkdir -p /home/iris/server_data/backup \
  && mkdir -p /home/iris/server_data/updates \
  && mkdir -p /home/iris/server_data/custom_assets \
  && mkdir -p /home/iris/server_data/datastore

WORKDIR /iriswebapp

COPY /scripts/set_password_policy.py .
COPY /scripts/iris-entrypoint.sh .
COPY /scripts/wait-for-iriswebapp.sh .
COPY /scripts/gunicorn-config.py .
COPY ./source .

RUN chmod +x iris-entrypoint.sh wait-for-iriswebapp.sh
#ENTRYPOINT [ "./iris-entrypoint.sh" ]
