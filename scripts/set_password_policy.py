import os
import json
import requests

# Konfiguration aus Umgebungsvariablen einlesen
API_URL="http://localhost:8000"
API_KEY = os.getenv("IRIS_ADM_API_KEY")

if not API_URL or not API_KEY:
    raise ValueError("Die Umgebungsvariablen API_URL und API_KEY müssen gesetzt sein.")

# Passwort-Policy festlegen
def set_password_policy():
    url = f"{API_URL}/manage/settings/update?cid=2"
    headers = {
        "Authorization": f"Bearer {API_KEY}",
        "Content-Type": "application/json"
    }
    payload = {
        "password_policy_min_length": os.environ.get('IRIS_SECURITY_PASSWORD_POLICY_MIN_LENGTH', '12'),
        "password_policy_special_chars": os.environ.get('IRIS_SECURITY_PASSWORD_POLICY_SPECIAL_CHARS', ''),
        "password_policy_lower_case": os.environ.get('IRIS_SECURITY_PASSWORD_POLICY_LOWER_CASE', True),
        "password_policy_digit": os.environ.get('IRIS_SECURITY_PASSWORD_POLICY_DIGITS', True),
        "password_policy_upper_case": os.environ.get('IRIS_SECURITY_PASSWORD_POLICY_UPPER_CASE', False)
    }

    try:
        response = requests.post(url, headers=headers, json=payload, verify=False)
        if response.status_code == 200:
            print("Passwort-Settings erfolgreich gesetzt.")
        else:
            print(f"Fehler beim Setzen der Passwort-Settings: {response.status_code} - {response.text}")
    except requests.RequestException as e:
        print(f"Fehler beim Verbinden mit der API: {e}")

# Hauptfunktion ausführen
if __name__ == "__main__":
    set_password_policy()
