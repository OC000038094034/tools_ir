import os


bind = os.environ.get('BIND', '0.0.0.0:8000')
backlog = int(os.environ.get('BACKLOG', 2048))

workers = int(os.environ.get('WORKERS', 1))
worker_class = os.environ.get('WORKER_CLASS', 'eventlet')
worker_connections = int(os.environ.get('WORKER_CONNECTIONS', 1000))
timeout = int(os.environ.get('WORKER_TIMEOUT', 10))
keepalive = int(os.environ.get('WORKER_KEEPALIVE', 10))
limit_request_fields = int(os.environ.get('LIMIT_REQUEST_FIELDS', 100))
limit_request_field_size = int(os.environ.get('LIMIT_REQUEST_FIELD_SIZE', 8190))
limit_request_line = int(os.environ.get('LIMIT_REQUEST_LINE', 4094))

spew = False

daemon = False
pidfile = os.environ.get('PIDFILE', '/tmp/gunicorn.pid')
umask = 0
user = None
group = None
tmp_upload_dir = None

errorlog = '-'
loglevel = os.environ.get('LOG_LEVEL', 'DEBUG')
accesslog = '-'
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'

proc_name = None

def pre_exec(server):
    server.log.info("Forked child, re-executing.")

def when_ready(server):
    server.log.info("Server is ready. Spawning workers")

def worker_int(worker):
    worker.log.info("worker received INT or QUIT signal")

    ## get traceback info
    import threading, sys, traceback
    id2name = {th.ident: th.name for th in threading.enumerate()}
    code = []
    for threadId, stack in sys._current_frames().items():
        code.append("\n# Thread: %s(%d)" % (id2name.get(threadId,""),
            threadId))
        for filename, lineno, name, line in traceback.extract_stack(stack):
            code.append('File: "%s", line %d, in %s' % (filename,
                lineno, name))
            if line:
                code.append("  %s" % (line.strip()))
    worker.log.debug("\n".join(code))

def worker_abort(worker):
    worker.log.info("worker received SIGABRT signal")
